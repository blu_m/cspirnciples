def list_products(num_list):
    product = 1  # Start out with 1
    for number in num_list:
        product = product * number
    return(product)

a_list = [1, 2, 3, 4, 5]
print(list_products(a_list))
