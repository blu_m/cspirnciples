def sum_evens(from_num, to_num):
    # Step 1: Initialize accumulator
    product = 1

    # Step 2: Get data
    numbers = range(from_num, to_num + 1, 2)

    # Step 3: Loop through the data
    for number in numbers:
        # Step 4: Accumulate
        product = product * number

    # Step 5: Return result
    return(product)

# Step 6: Print the result of calling the function
print(sum_evens(10,20))
