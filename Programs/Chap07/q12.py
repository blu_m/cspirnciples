def factorial(n):
    fact = 1                      # Set fact to passed value
    for i in range(1, n + 1):    # Count down from n to 1
        fact = fact * i  # Accumulate the product
    return(fact)

print(factorial(3))                                # Call and print to test

