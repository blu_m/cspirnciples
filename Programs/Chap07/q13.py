# Step 1: Initialize accumulator
product = 1  # initialize product to multiplicative identity

# Step 2: Get data
numbers = range(10, 21, 2)

# Step 3: Loop through the data
for number in numbers:
    # Step 4: Accumulate
    product = product * number

# Step 5: Process result
print(product)

