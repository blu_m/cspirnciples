def sum_evens(to_sum):
    sum = 0
    numbers = range(0, to_sum + 1, 2)
    for number in numbers:
        sum = sum + number
    return(sum)

print(sum_evens(3))
