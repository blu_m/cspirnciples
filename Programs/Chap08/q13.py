# Step 1: Initialize accumulator
product = 1  # Initialize to multiplication identity
# Step 2: Get data
numbers = 10
# Step 3: Loop through the data
while numbers < 21:
    # Step 4: Accumulate
   product = product * numbers
# Step 5: Process result
   print(product)
   numbers = numbers + 2
