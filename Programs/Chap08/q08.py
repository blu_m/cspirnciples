def sum_func(start, stop):
    sum = 0
    while sum < stop + 1:
        sum = sum + 1
    return sum

print(sum_func(1, 10))

