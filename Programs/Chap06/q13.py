def miles_drive(tankCapacity,the_amount_left,miles_a_gallon):
    tank_capacity = tankCapacity
    amount_left = the_amount_left
    num_gallons = tank_capacity * amount_left
    miles_per_gallon = miles_a_gallon
    num_miles = num_gallons * miles_per_gallon
    return(num_miles)

print(miles_drive(10,0.25,32))
