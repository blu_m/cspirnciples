def trip(mile,mile_per_gallon, price_a_gallon):
    miles = mile
    miles_per_gallon = mile_per_gallon
    num_gallons = mile / mile_per_gallon
    price_per_gallon = price_a_gallon
    total = num_gallons * price_a_gallon
    return(total)

print(trip(500,26,3.45))
