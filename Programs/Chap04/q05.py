age = int(input("How old are you? "))
# added a space so it looks neater when printed made it an int so people can only add numbers
name = input("What is your name? ")
# again a space to make it neater
print(f"Your name is {name} and you are {age} years old.")
#got rid of space in between print and (, made sure that age would print even though its an int, added f
