color = "red"
# this line in the CPS ebook has the code written like color = "red' the "'" at the end of red caused an error
name = "Carly"
# added double qoutations around Carly
print(f"Your name is {name} and your favorite color is {color}.")
#fixed line 4 which was indented and added "f" at the begining to make it shorter
