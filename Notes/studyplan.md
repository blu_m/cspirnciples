### Study Plan - 
# Information: 
1. **How is the APCSP exam scored?**
    * The exam has *2* parts, a *Create preformance Task* that is completed durring the course and a *multiple choice exam* that is given at the end of th course.
    * The Create Preformance Task is worth *30%*
    * The multiple choice exam accounts for *70%* 
2. **How many of the 70 multiple choice questions do i need to get correct to earn a 5, 4, or 3 on the exam?**
    * If you get a 6/6 on the Create Performance Task, then you need to get 32-50/70 to get a 3, a 51-59/70 for a 4 and a 60-70/70 for a 5.
    * If you get a 5/6 on the Create Performance Task, then you need to get 37-56/70 to get a 3, a 57-64/70 to get a 4 and a 65-70/70 to get a 5.
    * If you get a 4/6 on the Create Performance Task, then you need to get 42-60/70 to get a 3, a 61-69/70 to get a 4 and a 70/70 to get a 5.
    * If you get a 3/6 on the Create Performance Task, then you need to get a 47-65/70 to get a 3, a 66-70/70 to get a 4 and you can not get a 5 if you score a 3/6 on your Create Performance Task exam.
    * If you get a 2/6 on your Create Performance Task, then you need 52-70/70 to get a 3, you can not get a 4 or 5 if you get a 2/6 on your Create Performance Task.
    * If you get a 1/6 on your Create Performance Task, then you need a 57-70/70 to get a 3, you can not get a 4 or 5 if you get a 1/6 on your Create Performance Task.
    * if you get a 0/6 on your Create Performance Task, then you need a 62-70/70 to get a 3, you can not get a 4 or 5 if you get a 0/6 on your Create Performance Task.
3. **What is the Create Preformance Task and how is it scored?**
    * The Create Performance Task is where students are provided *12 hours* to complete the performance task. **This task focuses specifically on the creation of a computer program, accompanied by a video and written response.**
4. **On which of the 5 Big Ideas did I score best?**
    * I scored best on Big Idea 3 with 14/28 
5. **On whih do i need the most improvement?**
    * I need improvement on Big Idea 4 with 2/6
6. **What online resources are available to help me prepare for the exam in each of the Big Idea areas?**
    * Some online recources availible to me is the College Board website, Khan Academy, and more recources.

