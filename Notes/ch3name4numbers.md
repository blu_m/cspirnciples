* Chapter 3: Names for Numbers 

## Objectives - 

* Understand concept of variable
* How to assign value to variable
* Reuse variables

## Definitions - 

### Variable - 
        Is a space in computer memory that can repersent a value. (ex. The score in a computer game)
### Assignment -
        Setting a variables value. (ex x = 5 )
### Modulo - 
        Remainder operator. (ex. 4 % 2 = 0 because 4/2 has no remainder)
### Run a program - 
        Telling the computer to execute each step of the program as fast as possible.
### Arithmetic Expression - 
        Contains a mathmatical operator. (ex. - for subtraction, * for multiplacation) 
### Assignment Dyslexia - 
        Is putting the variable on the right and the value on the left. (ex.5 = x is not a legal statment)
### Integer Division - 
        When you divide one integer by another.
### Tracing - 
        Keeping track of the variables in the program and how their values change as the statments are executed.

        

## Leagal Variable Names - 
* Must start with a letter (commonly lowercase but uppercase is okay too) or an _ underscore.
* Can contain numbers, just not in the first character.
* Cannot be a python key word.
* Case sensitive, Case and case are NOT the same!
* No spaces.( to put 2 words together you could do secondPlace)
 

## Python Key Words - 
* and           
* def
* elif
* for
* if
* import
* in
* not
* or
* return
* while 

## Extra Notes - 
* print cant take an imput (variable name inside parentheses) whos value will be displayed.
* print can also print a string (ex. print("hello world") would print hello world on the screen exactly as you see it) 
  
