# Presentation Notes
- Peresentations on how to earn college credit from the APCSP Exam

## Group C -
- They give us the 5 big ideas.
- Big Idea 1 and 4 arent worth much.
- Take practice test, especially review ones taken in class.
- Showed pass rates and study time requirements, as well as the difuclty rating 
### Online Resources - 
- Khan Academy, which uses javascript but is similar to our course. We used it at the start of the year.
- The College Board, has tips and some grading criteria.
- Test Guides Website, where your able to take 4 practice test for free and more helpful things.
### Personal Assesment -

### Practice Problems - 
1. You develop a program that lets you use different operations (addition, subtraction, multiplication, division, etc.) on a set of numbers derived from a database. However, the actual test results give a random value, which leads you to suspect that there is a randomizing variable in your code.

- [x] Rerunning the code
- [ ] Code Tracing
- [ ] Using a code visualizer
- [ ] Usings DISPLAY statements at different points of code

2. Musicians record their songs on a computer. When they listen to a digitally saved copy of their song, they find the sound quality lower than expected. Which of the following could be a possible reason why this difference exists?

- [ ] The file ws moved from the original location, causing some information to be lost
- [ ] The recording was done through the lossless compression technique
- [ ] The song file was saved with higher bits per second
- [x] The song file was saved with lower bits per second

3. A school had a 90% pass rate for students that took the last AP exam. The school wants to use this achievement to advertise its services to families of prospective students. 
Which of the following methods would be most effective in delivering this information to the families in a summarized manner?

- [ ] Email the prospective families that have middle-school-age chilren
- [ ] Create a report based on the student bodys overall preformance for a marketing pamphlet
- [x] Post an interactive pie chart about the topics and scores of passing students on the schools website
- [ ] Post the results of the passing students on social media sites

Resource used - [Test-Guide Test 1](https://www.test-guide.com/quiz/ap-comp-sci-1.html)
    
