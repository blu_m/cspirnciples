# Written Response (Question 3) for Create Performance Task

## 3a. Provide a written response that does all three of the following:

1. Describes the overall purpose of the program
> To make a fun game that meets the six requirements
>
>
2. Describes what functionality of the program is demonstrated in the
   video.
> The player is a small circle, and he moves arround. As he moves around the small squares follow the circle, the circle can teleport. 
> If the squares touch in anyway they stop what they are doing and dont move for the rest of the game.
>
3. Describes the input and output of the program demonstrated in the
   video.
> You press keys which is the input and specific keys make the player move which is the output.
>
>

## 3b. Provide a written response to the following:
 
Capture and paste two program code segments you developed during the
administration of this task that contain a list (or other collection
type) being used to manage complexity in your program.

1. The first program code segment must show how data have been
   stored in the list.
> ![List](image\robots_list.png)
>
>
2. The second program code segment must show the data in the same list being
   used, such as creating new data from the existing data or accessing
   multiple elements in the list, as part of fulfilling the program’s purpose.
> ![List being used](robot_list_used.png)
>
>

## Then provide a written response that does all three of the following: 

1. Identifies the name of the list being used in this response
> robots
>
>
2. Describes what the data contained in the list represent in your
   program
> the data contained in the list when created holds nothing, though its made to store trhe location of robots
>
>
3. Explains how the selected list manages complexity in your program code by
   explaining why your program code could not be written, or how it would be
   written differently, if you did not use the list
> the lis make sit so there is no need for individual variables. this make sit so we can have more robots
>
>

## 3c. Provide a written response to the following:

Capture and paste two program code segments you developed during the
administration of this task that contain a student-developed procedure that
implements an algorithm used in your program and a call to that procedure.

1. The first program code segment must be a student-developed
   procedure that
   - Defines the procedure’s name and return type (if necessary)
   - Contains and uses one or more parameters that have an effect on the
     functionality of the procedure
   - Implements an algorithm that includes sequencing, selection, and
     iteration
> ![3c](line113.png)
>
> The procedures name is robot_crashed(the_bot), the perameter is (the_bot). The programs purpose is to see if the robot has crashed. selection and sequencing are shown through the if statments and iterations is shown through the loop.
>
>
2. The second program code segment must show where your student-developed
   procedure is being called in your program.
> ![3cp2](3cp2.png)
> robot_crashed(the_both) is called in check_collisions()
>
>

## Then provide a written response that does both of the following:

3. Describes in general what the identified procedure does and how it
   contributes to the overall functionality of the program
> robot_crashed(the_bot) checks if a robot makes  contact with another robot, if it does it returns true and false if it has not. The player wins the game by making the robots crash into each other. 
>
>
4. Explains in detailed steps how the algorithm implemented in the identified
   procedure works. Your explanation must be detailed enough for someone else
   to recreate it.
> The procedure takes the parameter the_bot and then compares it to a_bot every time the loop runs through. The loop compares the x's and y's of the variables, looking for the same 'coordinates'. This looks for a collision. If the variables are not the same meaning the robots do not hit each other, the procedure will return false. If the variables are the same, then the robots have collided and the procedure will return true. 
>
>
>

## 3d. Provide a written response that does all three of the following:

1. Describes two calls to the procedure identified in written response 3c. Each
   call must pass a different argument(s) that causes a different segment of
   code in the algorithm to execute.
>
> First call:
> ![3d](3d1.png)
> this makes sure that the player wont be placed in the same place as a robot, this is in safely_place_player()
>
>
> Second call:
> ![3d2](3d2.png) 
> the check_collisions() checks to see if there was a collision between a player and a robot or 2 robots.
>
>
2. Describes what condition(s) is being tested by each call to the procedure
>
> Condition(s) tested by the first call:
> conditions is the intended placement of the player and placement of the robots
>
>
> Condition(s) tested by the second call:
> the conditions are if thr robots share the same cordinates and the placement o
f robots
>
>
>
3. Identifies the result of each call
>
> Result of the first call:
> if a player is placed at the same cordinates of a robot then it woulf get new cordinates, if not then the player would be placed and the game begins 
>
>
>
> Result of the second call:
> if the robots collide then 1 becomes junk and the other gets removed
>
>
>
