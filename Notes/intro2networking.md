# Chapter 1 - Introduction

### Definitions - 
Address
: A number that is assigned to a computer so that messages can be routed to the computer.

Hop
: A single physical network connection. A packet on the Internet will typically make several “hops” to get from its source
computer to its destination.

LAN - Local Area Network
: A network covering an area that is
limited by the ability for an organization to run wires or the power
of a radio transmitter.

Leased Line
: An “always up” connection that an organization
leased from a telephone company or other utility to send data
across longer distances.

Operator - Telephone
: A person who works for a telephone company and helps people make telephone calls.

Packet
: A limited-size fragment of a large message. Large messages or files are split into many packets and sent across the
Internet. The typical maximum packet size is between 1000 and
3000 characters.

Router
: A specialized computer that is designed to receive incoming packets on many links and quickly forward the packets on the
best outbound link to speed the packet to its destination.

Store-and-forward Network
:  A network where data is sent
from one computer to another with the message being stored
for relatively long periods of time in an intermediate computer
waiting for an outbound network connection to become available.

WAN - Wide Area Network
: A network that covers longer distances, up to sending data completely around the world. A WAN
is generally constructed using communication links owned and
managed by a number of different organizations.
## Notes - 
- Messages between computers could be short, medium, or long.
- A message would arrive at an intermediate computer, be stored for a while (could be hours), and then be forwarded one more connection.
- They started to include special-purpose computers that were specialized in moving packets, they were called "Interface Message Processors" or "IMPs".
- Later on "IMPs" were called "routers" because their purpose was to route the packets they recived towards their ultimate destination.
- Multiple computers at one location were connected together in a LAN, using physical wiring, you would connect a router to the local area network.
- It was important to know the source and destination computers for every message, so each computer was given a unique name or number that was called the "address".
   
# Chapter 2 - Network Architecture

### Definitions - 

Client
: In a networked application, the client application is the
one that requests services or initiates connections.

Fiber Optic
:  A data transmission technology that encodes data
using light and sends the light down a very long strand of thin glass or plastic. Fiber optic connections are fast and can cover very long distances.

Offset
: The relative position of a packet within an overall message or stream of data.

Server
: In a networked application, the server application is the
one that responds to requests for services or waits for incoming
connections.

Window Size
: The amount of data that the sending computer is
allowed to send before waiting for an acknowledgement.

# Chapter 3 - Link Layer

### Definitions - 

Base Station
: Another word for the first router that handles your
packets as they are forwarded to the Internet.

Broadcast
: Sending a packet in a way that all the stations connected to a local area network will receive the packet.

Gateway
: A router that connects a local area network to a wider
area network such as the Internet. Computers that want to send
data outside the local network must send their packets to the
gateway for forwarding.

MAC Address
: An address that is assigned to a piece of network
hardware when the device is manufactured.

Token
: A technique to allow many computers to share the same
physical media without collisions. Each computer must wait until
it has received the token before it can send data.

## Notes - 
- The link layer is called the "lowest layer" because its the closest to the physical network media.
- The Link layer transmits data using a wire, a
fiber optic cable, or a radio signal.
-  A rogue computer could also be
listening to and capturing your packets, perhaps getting ahold of
important data like bank account numbers or passwords to online
services.
- MAC stands for Media Access Control
- MAC address is like a "from" and/or "to" address on a post card.
- Link layer technologies can ignore all of the issues
that are handled by the layers above the Link layer.

# Chapter 4 - Internetworking Layer

### Definitions - 

Core Router
: A router that is forwarding traffic within the core of the Internet.

DHCP - Dynamic Host Configuration Protocol
: DHCP is how a portable computer gets an IP address when it is moved to a new
location.

Edge Router
: A router which provides a connection between a local network and the Internet. Equivalent to “gateway”.

IP Address
: A globally assigned address that is assigned to a computer so that it can communicate with other computers that have IP addresses and are connected to the Internet. To simplify routing in the core of the Internet IP addresses are broken into Network Numbers and Host Identifiers. An example IP address might be “212.78.1.25”.

Host Identifier
: The portion of an IP address that is used to identify a computer within a local area network.

NAT - Network Address Translation
: The portion of an IP address that is used to identify which local network the computer is connected to.

Packet Vortex
: An error situation where a packet gets into an infinite loop because of errors in routing tables.

RIR - Regional Internet Registy
: The five RIRs roughly correspond to the continents of the world and allocate IP address for the major geographical areas of the world.

Routing Tables
: Information maintained by each router that keeps track of which outbound link should be used for each network number.

Time To Live - TTL
: A number that is stored in every packet that is reduced by one as the packet passes through each router. When the TTL reaches zero, the packet is discarded.

Tarceroute
: A command that is available on many Linux/UNIX systems that attempts to map the path taken by a packet as it moves from its source to its destination. May be called “tracert” on Windows systems.

Two-connected Network 
: A situation where there is at least two possible paths between any pair of nodes in a network. A two-connected network can lose any single link without losing overall connectivity.

# Chapter 5 - The Domain Name System

### Definitions - 

DNS - Domain Name System
:  A system of protocols and servers
that allow networked applications to look up domain names and
retrieve the corresponding IP address for the domain name.

Domain Name
: A name that is assigned within a top-level domain. For example, khanacademy.org is a domain that is assigned
within the “.org” top-level domain.

ICANN - International Corporation for Assigned Network Names and Numbers
:  Assigns and manages the top-level domains for
the Internet.

Registrar
: A company that can register, sell, and host domain
names.

Subdomain
: A name that is created “below” a domain
name. For example, “umich.edu” is a domain name and
both “www.umich.edu” and “mail.umich.edu” are subdomains
within “umich.edu”.

TLD - Top Level Domain
: The rightmost portion of the domain
name. Example TLDs include “.com”, “.org”, and “.ru”. Recently,
new top-level domains like “.club” and “.help” were added.

## Notes - 
- Domain names allow end users to use symbolic names for servers instead of numeric Internet Protocol addresses.
- Once a domain name is assigned to an organization, the controlling organization is allowed to assign subdomains within the
domain.
-  Domains ending in .com and .org can be purchased by individuals
- We read IP addresses from left to right (left is most general and right is most specific), domains on the other hand read from right to left

# Chapter 6 - Transport Layer

### Definitions - 

Acknowledgement
: When the receiving computer sends a notification back to the source computer indicating that data has been received.

Buffering
: Temporarily holding on to data that has been sent or received until the computer is sure the data is no longer needed.

Listen
: When a server application is started and ready to accept incoming connections from client applications.

Port
: A way to allow many different server applications to be waiting for incoming connections on a single computer. Each application listens on a different port. Client applications make connections to well-known port numbers to make sure they are talking to the correct server application.

## Notes - 
- the Transport layer adds a small amount of data to each packet to help solve the problems of packet reassembly and retransmission.
- Part of the goal of a layered architecture is to break an overly complex problem into smaller subproblems.
- the Transport layer in the sending computer only sends a certain amount of data before waiting for an acknowledgement from the Transport layer on the destination computer that the packets were received. when it does this its called "window size"
- the window size is adjustable so that the transmitting computer can send large amounts of data quickly over fast connections that have light loads and when sending data to slow and heavily loaded links, they can send small amounts that dont overwhelm the network.
- if the sending computer doesnt reacive acknowledgement from the destination computer that it recieved the packets, then it stops sending data packets

# Chapter 7 - Application Layer

### Definitions - 

HTML - HyperText Markup Language
:  A textual format that marks up text using tags surrounded by less-than and greater-than characters. Example HTML looks like: <p>This is <strong>nice</strong></p>. 

HTTP - HyperText Transport Protocol
: An Application layer protocol that allows web browsers to retrieve web documents from web servers.

IMAP - Internet Message Access Protocol - 
: A protocol that allows mail clients to log into and retrieve mail from IMAP-enabled mail servers.

Flow Control 
: When a sending computer slows down to make sure that it does not overwhelm either the network or the destination computer. Flow control also causes the sending computer to increase the speed at which data is sent when it is sure that the network and destination computer can handle the faster data rates.

Socket
: A software library available in many programming languages that makes creating a network connection and exchanging data nearly as easy as opening and reading a file on your computer

Satus Code
: One aspect of the HTTP protocol that indicates the overall success or failure of a request for a document. The most well-known HTTP status code is “404”, which is how an HTTP server tells an HTTP client (i.e., a browser) that it the requested document could not be found.

Telent
:  A simple client application that makes TCP connections to various address/port combinations and allows typed data to be sent across the connection. In the early days of the Internet, telnet was used to remotely log in to a computer across the network.

Web Browser
: A client application that you run on your computer to retrieve and display web pages.

Web Server
:  An application that deliver (serves up) Web pages

## Notes - 
- the rest of the layers are there to make it so the applications running in the application layer can focus the application problem that needs to be solved. 
- the application layer is where the networked software like web browsers, mail programs, video players, or networked videos operate.
- the set of rules that govern how we communicate is called a "protocall"
- computers like percision
- the internet was created in 1985 bythe NSFNet project and the precursor to the NSFNet called the ARPANET was brought up in 1969.

# Chapter 8 - Secure Transport Layer

### Definitions - 

Asymmetric Key
: An approach to encryption where one (public) key is used to encrypt data prior to transmission and a different (private) key is used to decrypt data once it is received.

Certificate Authority
: An organization that digitally signs public keys after verifying that the name listed in the public key is actually the person or organization in possession of the public key.

Ciphertext
: A scrambled version of a message that cannot be read without knowing the decryption key and technique.

Decrypt
: The act of transforming a ciphertext message to a plain text message using a secret or key.

Encrypt
: The act of transforming a plain text message to a ciphertext message using a secret or key.

Plain Text
: A readable message that is about to be encrypted before being sent.

Private Key
: The portion of a key pair that is used to decrypt transmissions.

Public Key
: The portion of a key pair that is used to encrypt transmissions.

Shared Secret
:  An approach to encryption that uses the same key for encryption and decryption.

SSL - Secure Sockets Layer
: Secure Sockets Layer. An approach that allows an application to request that a Transport layer connection is to be encrypted as it crosses the network. Similar to Transport Layer Security (TLS).

TLS - Transport Layer Security 
: Transport Layer Security. An approach that allows an application to request that a Transport layer connection is to be encrypted as it crosses the network. Similar to Secure Sockets Layer (SSL).

## Notes - 
-  There are subtle differences between SSL and TLS but they both encrypt data at the Transport layer.
- the invention of public/private key encryption solved the key distribuation problem of shared-secret encryption approaches.
- the concept of asymmetric key encryption was developed in 1970s

