# Chapter 7 - Computers can Repeat Steps
### Learning objectives: 
   *  Use a **for**  loop to repeat code.
   *  Use **range** to create a list of numbers.
### Definitions - 
Loop
: A Loop tells the computer to repeat a statment. 

Accumulator Pattern
: The accumulator patter is a set of steps that processes a list of values. One example of an accumulator is the code to sum a list of nummbers.

Body of a Loop 
: The body of a loop is a statement or set of statements to be repeated in a loop. Python uses indention to indicate the body of a loop.

Indention
: Indention means that the text on the line has spaces at the beginning of the line so that the text doesn’t start right at the left boundary of the line. In Python indention is used to specify which statements are in the same block. For example the body of a loop is indented 4 more spaces than the statement starting the loop.

Iteration
: Iteration is the ability to repeat a step or set of steps in a computer program. This is also called looping.

List 
: A list holds a sequence of items in order. An example of a list in Python is [1, 2, 3].

### Python Keywords & Functions - 
def 
: The def keyword is used to define a procedure or function in Python. The line must also end with a : and the body of the procedure or function must be indented 4 spaces.

for 
: A for loop is a programming statement that tells the computer to repeat a statement or a set of statements. It is one type of loop.

print 
: The print statement in Python will print the value of the items passed to it.

range 
: The range function in Python returns a list of consecutive values. If the range function is passed one value it returns a list with the numbers from 0 up to and not including the passed number. For example, range(5) returns a list of [0, 1, 2, 3, 4]. If the range function is passed two numbers separated by a comma it returns a list including the first number and then up to but not including the second number. For example, range(1, 4) returns the list [1, 2, 3]. If it is passed three values range(start,end,step) it returns all the numbers from start to one less than end changing by step. For example, range(0, 10, 2) returns [0, 2, 4, 6, 8].
   * Definitions can be found here [Ch7 - Definitions](https://www.openbookproject.net/books/StudentCSP/CSPRepeatNumbers/ch7_summary.html)


## Notes - 
### Repeating with Numbers:
   *  A for loop is one type of loop or way to repeat a statment or set of statments in a program. 
   *  A for loop will use a variable and make the variable take each of the values in a **list** of numbers one at a time.
   *  A list holds values in an order.
   *  Indented means that text on the line has spaces at the beginning of the line so that the text doesnt start right at the left boundry.
   *  Line 3 (in the code below) is the start of the for loop and line 4 is the **body** of the loop.
`sum = 0
things_to_add = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
for number in things_to_add:
    sum = sum + number
print(sum)`
   *  The body of the loop is repeated for each value in the list things to add .
   -  Information above found on [Ch7 - Repeating with Numbers](https://www.openbookproject.net/books/StudentCSP/CSPRepeatNumbers/repeatNums.html)
### What is a List?:
   *  A **list** holds items in an order.
   *  Python list are inclosed in [ and ] and can have values seperated by commas like **[1,2,3,4,5,6,7,8,9,10]**
   *  A list has an order and each list item has a position in the list, like the first item in a list or the last item in a list.
   -  Information above found from [Ch7 - What is a List](https://www.openbookproject.net/books/StudentCSP/CSPRepeatNumbers/list.html)
### The Range Function:
   * The range function to loop over sequence of numbers.
   * If the range function is called with a single positive integer, it will generate all the integer values from 0 to one less than the number it was passed and assign them one at a time to the loop variable.
   * Information above found from [Ch7 - The Range Function](https://www.openbookproject.net/books/StudentCSP/CSPRepeatNumbers/range.html)
### There's a Pattern Here!
   * Theres a pattern in these programs, a patern that is common when processing data. This is called **Accumulator Pattern**.
   * In the first programs we *accumulated* the values into the variable sum. 
   * In last programs we *accumulated* a product into the variable product.
   * Here are the five steps in this pattern.
   1. Set the accumulator variable to its initial value. This is the value we want if there is no data to be processed.
   2. Get all the data to be processed.
   3. Step through all the data using a for loop so that the variable takes on each value in the data.
   4. Combine each piece of the data into the accumulator.
   5. Do something with the result.    
   * Information above found from [Ch7 - There's a Pattern Here!](https://www.openbookproject.net/books/StudentCSP/CSPRepeatNumbers/accumPattern.html)
 



